
# Policy Document - what terraform can do

data "aws_iam_policy_document" "terraform" {
  statement {
    actions   = [
                 "eks:*",
                "cloudformation:*",
                "elasticfilesystem:*",
                "es:*",
                "cloudformation:*",
                "lambda:*",
                "apigateway:*",
                "ec2:*",
                "s3:*",
                "sns:*",
                "states:*",
                "sts:*",
                "ssm:*",
                "sqs:*",
                "iam:*",
                "elasticloadbalancing:*",
                "autoscaling:*",
                "cloudwatch:*",
                "route53:*",
                "ecr:*",
                "logs:*",
                "ecs:*",
                "application-autoscaling:*",
                "logs:*",
                "events:*",
                "dynamodb:*",
                "dlm:*",
                "acm:*"
    ]
    resources = ["*"]
    effect = "Allow"
  }
}

# Create the policy using the Policy Document created

 resource "aws_iam_policy" "terraform_policy" {
   name        = "terraform_policy"
   path        = "/terraform/"     
   description = "terraform policy"
   policy = data.aws_iam_policy_document.terraform.json
 }

