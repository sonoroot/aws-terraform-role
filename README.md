## Mike has no power

```
❯ export AWS_PROFILE=mike
❯ aws s3 ls

An error occurred (AccessDenied) when calling the ListBuckets operation: Access Denied
```

## Assume terraform Role via CLI
```
aws_credentials=$(aws sts assume-role --role-arn arn:aws:iam::xxxxxxxxxxxx:role/terraform_role --role-session-name "terraform-god-mode")

export AWS_ACCESS_KEY_ID=$(echo $aws_credentials|jq '.Credentials.AccessKeyId'|tr -d '"')
export AWS_SECRET_ACCESS_KEY=$(echo $aws_credentials|jq '.Credentials.SecretAccessKey'|tr -d '"')
export AWS_SESSION_TOKEN=$(echo $aws_credentials|jq '.Credentials.SessionToken'|tr -d '"')
```

## Mike has power

```
❯ export AWS_PROFILE=mike
❯ aws s3 ls

- buckets...
- buckets...

```

