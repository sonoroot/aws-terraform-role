

# Iam Role

resource "aws_iam_role" "terraform_role" {
  name = "terraform_role"

  assume_role_policy = data.aws_iam_policy_document.terraform_role_policy.json

}

 # IAM Role Policy Document

 data "aws_iam_policy_document" "terraform_role_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::xxxxxxxxxxxx:user/mike"]
    }
  }
}

# Attach policy

resource "aws_iam_role_policy_attachment" "terraform_attach" {
  role       = aws_iam_role.terraform_role.name
  policy_arn = aws_iam_policy.terraform_policy.arn
}
